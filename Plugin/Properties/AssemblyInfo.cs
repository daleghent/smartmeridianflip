﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// The name of your plugin
[assembly: AssemblyTitle("Smart Meridian Flip")]
// A short description of your plugin
[assembly: AssemblyDescription("A meridian flip trigger that considers obstructions at different declinations to optimize imaging downtime.")]
[assembly: AssemblyConfiguration("")]
//Your name
[assembly: AssemblyCompany("Francesco Meschia")]
//The product name that this plugin is part of
[assembly: AssemblyProduct("fmeschia.NINA.Plugin.SmartMeridianFlip")]
[assembly: AssemblyCopyright("Copyright © 2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6d0e07f2-8773-4229-bf2c-f451e53f677a")]

//The assembly versioning
//Should be incremented for each new release build of a plugin
[assembly: AssemblyVersion("1.0.1.5")]
[assembly: AssemblyFileVersion("1.0.1.5")]

//The minimum Version of N.I.N.A. that this plugin is compatible with
[assembly: AssemblyMetadata("MinimumApplicationVersion", "2.0.0.2050")]

//Your plugin homepage - omit if not applicaple
//[assembly: AssemblyMetadata("Homepage", "https://example.com/")]
//The license your plugin code is using
[assembly: AssemblyMetadata("License", "MPL-2.0")]
//The url to the license
[assembly: AssemblyMetadata("LicenseURL", "https://www.mozilla.org/en-US/MPL/2.0/")]
//The repository where your pluggin is hosted
[assembly: AssemblyMetadata("Repository", "https://bitbucket.org/fmeschia/smartmeridianflip/")]

//Common tags that quickly describe your plugin
[assembly: AssemblyMetadata("Tags", "Sequencer,Trigger,Meridian Flip,Declination,Meridian Delay,Meridian Pause")]

//The featured logo that will be displayed in the plugin list next to the name
//[assembly: AssemblyMetadata("FeaturedImageURL", "https://nighttime-imaging.eu/wp-content/uploads/2019/02/Logo_Nina.png")]
//An example screenshot of your plugin in action
//[assembly: AssemblyMetadata("ScreenshotURL", "https://bitbucket.org/Isbeorn/nina.plugins/downloads/Starlock2.png")]
//An additional example screenshot of your plugin in action
//[assembly: AssemblyMetadata("AltScreenshotURL", "https://bitbucket.org/Isbeorn/nina.plugins/downloads/Instruction.png")]
[assembly: AssemblyMetadata("LongDescription", @"The Smart Meridian Flip trigger can enforce different safe tracking limits for the West and East side of the meridian at different declinations.

This can be useful to optimize the imaging time when, for instance, the mount needs to stop tracking before the meridian to prevent the scope from hitting the pier, but only for certain values of declination(typically where the back of the scope is closer to the pier / tripod).

To use this plugin, configure it and insert it as a sequence trigger in the Advanced Sequencer.

This plugin needs to be configured with a file that describes the obstructions present on the West and East side at various declination angles. It can process Astro - Physics APCC `.mlm` files directly, or it can process `.obs` files that follow this format:

    < declination 1 >< West limit in minutes >< East limit in minutes >
    < declination 2 >< West limit in minutes >< East limit in minutes >
    ...
    < declination n >< West limit in minutes >< East limit in minutes >

The West - side and East - side limits are in minutes, and they indicate the maximum time before the meridian(West - side) and minimum time after the meridian(East - side) that it's safe for the telescope to be at, for a given declination. 
The West-side obstruction can also be a negative value, to indicate how far the telescope can safely track past the meridian.

Since a meridian flip is possible both on the arc of meridian between the Pole and the horizon opposite to the Pole(""above"" the Pole), and on the arc between the Pole and the horizon next to the Pole(""below"" the Pole), the declination range used in the file is expanded from the traditional -90 - +90 degree range. 

Values between - 90 degrees and 90 degrees will be used to represent the arc of meridian ""above"" the Pole, and values between 90 and 180 degrees will be used to represent the arc ""below"" the Pole. So, 85 degrees of celestial declination on the arc from the Pole to the horizon next to it will be represented in the file by 95 degrees(180 - 85), 60 degrees of celestial declination will be represented by 120 degrees (180 - 60), and so on.

The file must contain at least two lines, corresponding to different declinations. Declination values between listed values will be interpolated.

An example of an obstructions file:

    # File Example
    # Any line starting with '#' is a comment
    #
    # The following line means that, at declination zero, the telescope can track 10 minutes after meridian, but can 
    # also safely flip right after the meridian
    0 - 10 0
    # This line says that at dec. +40 deg the telescope can safely track up to the meridian, and flip right after 
    # the target has passed the meridian
    40 0 0
    # This line says that at dec. +45 deg the telescope should stop tracking at least 20 minutes before
    # the meridian, can can flip no earlier than 30 minutes after the target has crossed the meridian
    45 20 30
    # This line says that at dec. +50 deg the telescope can again safely track up to the meridian, and can flip
    # right after the target has crossed the meridian
    50 0 0
    # The same limits continue to the pole:
    90 0 0
    # And to the equator ""below"" the pole:
    180 0 0


")]