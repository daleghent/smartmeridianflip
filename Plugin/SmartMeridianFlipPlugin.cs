﻿#region "copyright"

/*
    Copyright © 2021 Francesco Meschia <francesco.meschia@gmail.com>
    Copyright © 2016 - 2021 Stefan Berg <isbeorn86+NINA@googlemail.com> and the N.I.N.A. contributors

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#endregion "copyright"

using NINA.Core.Utility;
using NINA.Plugin;
using NINA.Plugin.Interfaces;
using NINA.Profile;
using NINA.Profile.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;

namespace fmeschia.NINA.Plugin.SmartMeridianFlip 
{

    public class SmartMeridianFlipMediator
    {

        private SmartMeridianFlipMediator() { }

        private static readonly Lazy<SmartMeridianFlipMediator> lazy = new Lazy<SmartMeridianFlipMediator>(() => new SmartMeridianFlipMediator());

        public static SmartMeridianFlipMediator Instance { get => lazy.Value; }
        public void RegisterPlugin(SmartMeridianFlipPlugin plugin) {
            this.Plugin = plugin;
        }

        public SmartMeridianFlipPlugin Plugin { get; private set; }
    }

    /// <summary>
    /// This class connects the Smart Meridian Flip trigger to NINA via the plugin system.
    /// </summary>
    [Export(typeof(IPluginManifest))]
    public class SmartMeridianFlipPlugin : PluginBase, ISettings, INotifyPropertyChanged
    {
        private IPluginOptionsAccessor pluginSettings;
        private IProfileService profileService;
        public CustomObstruction obstructions;

        [ImportingConstructor]
        public SmartMeridianFlipPlugin(IProfileService profileService) {
            if (Properties.Settings.Default.UpdateSettings) {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.UpdateSettings = false;
                CoreUtil.SaveSettings(Properties.Settings.Default);
            }
            SmartMeridianFlipMediator.Instance.RegisterPlugin(this);
            this.pluginSettings = new PluginOptionsAccessor(profileService, Guid.Parse(this.Identifier));
            this.profileService = profileService;
            IDictionary<double, double[]> defaultObstructions = new Dictionary<double, double[]>();
            double[] obstructions = new double[2];
            obstructions[0] = profileService.ActiveProfile.MeridianFlipSettings.PauseTimeBeforeMeridian;
            obstructions[1] = profileService.ActiveProfile.MeridianFlipSettings.MaxMinutesAfterMeridian;
            defaultObstructions.Add(0, obstructions);
            defaultObstructions.Add(180, obstructions);
            this.obstructions = new CustomObstruction(defaultObstructions);
            loadObstructionFile();
            OpenObstructionsFilePathDiagCommand = new RelayCommand(OpenObstructionsFilePathDiag);
            profileService.ProfileChanged += ProfileService_ProfileChanged;
        }

        public override Task Teardown() {
            // Make sure to unregister an event when the object is no longer in use. Otherwise garbage collection will be prevented.
            profileService.ProfileChanged -= ProfileService_ProfileChanged;
            return base.Teardown();
        }

        private void ProfileService_ProfileChanged(object sender, EventArgs e) {
            // Rase the event that this profile specific value has been changed due to the profile switch
            loadObstructionFile();
            RaisePropertyChanged(nameof(ObstructionFile));
        }

        public ICommand OpenObstructionsFilePathDiagCommand { get; private set; }

        protected void loadObstructionFile() {
            if (!string.IsNullOrEmpty(SmartMeridianFlipMediator.Instance.Plugin.ObstructionFile)) {
                Logger.Debug($"Loading obstruction file {SmartMeridianFlipMediator.Instance.Plugin.ObstructionFile}");
                try {
                    this.obstructions = CustomObstruction.FromFile(SmartMeridianFlipMediator.Instance.Plugin.ObstructionFile, profileService.ActiveProfile.AstrometrySettings.Latitude);
                } catch (FileNotFoundException) {
                    SmartMeridianFlipMediator.Instance.Plugin.ObstructionFile = string.Empty;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged([CallerMemberName] string propertyName = null) {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string ObstructionFile {
            get {
                var obstructionFilename = pluginSettings.GetValueString(nameof(ObstructionFile), string.Empty);
                if (string.IsNullOrEmpty(obstructionFilename)) {
                    obstructionFilename = Properties.Settings.Default.ObstructionFilename;
                    if (string.IsNullOrEmpty(obstructionFilename) || File.Exists(obstructionFilename)) {
                        obstructionFilename = String.Empty;
                        Properties.Settings.Default.ObstructionFilename = String.Empty;
                    }
                    pluginSettings.SetValueString(nameof(ObstructionFile), obstructionFilename);
                } else {
                    if (!File.Exists(obstructionFilename)) {
                        obstructionFilename = string.Empty;
                        pluginSettings.SetValueString(nameof(ObstructionFile), obstructionFilename);
                    }
                }
                return obstructionFilename;
            }
            set {
                pluginSettings.SetValueString(nameof(ObstructionFile), value);
                if (!string.IsNullOrEmpty(value))
                    if (File.Exists(value)) {
                        loadObstructionFile();
                    } 
                RaisePropertyChanged();
            }
        }

        protected void SetDefaultValues() {
            ;
        }

        private void OpenObstructionsFilePathDiag(object obj) {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();

            dialog.FileName = string.Empty;
            dialog.Filter = "Obstructions File|*.obs|APCC meridian file|*.mlm";

            if (dialog.ShowDialog() == true) {
                ObstructionFile = dialog.FileName;
            }
        }

    }
}